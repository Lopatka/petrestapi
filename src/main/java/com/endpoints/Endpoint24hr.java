package com.endpoints;

import org.jetbrains.annotations.NotNull;

import static com.Constants.ENDPOINT_TICKER_24HR;

enum Endpoint24hrType {
    FULL,
    MINI
}

public class Endpoint24hr extends Endpoint {
    private Endpoint24hrType type = Endpoint24hrType.FULL;

    public Endpoint24hr() {
        this.endpoint = ENDPOINT_TICKER_24HR;
    }

    public void setTypeMini() {
        this.type = Endpoint24hrType.MINI;
    }

    public void setTypeFull() {
        this.type = Endpoint24hrType.FULL;
    }

    public String getEndpoint() {
        return this.endpoint;
    }

    public String getEndpoint(String symbol) {
        return endpoint + "?type=" + type + "&symbol=" + symbol;
    }

    public String getEndpoint(String @NotNull [] symbols) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");
        for (int i = 0; i < symbols.length; i++) {
            stringBuilder.append("\"");
            stringBuilder.append(symbols[i]);
            stringBuilder.append("\"");
            if (i != symbols.length - 1){
                stringBuilder.append(",");
            }
        }
        stringBuilder.append("]");
        return endpoint + "?type=" + type + "&symbols=" + stringBuilder;
    }
}
