package com.tickers;

import com.Parser;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

enum DayStatisticFull {
    SYMBOL("symbol"),
    PRICE_CHANGE("priceChange"),
    PRICE_CHANGE_PERCENT("priceChangePercent"),
    WEIGHTED_AVG_PRICE("weightedAvgPrice"),
    PREV_CLOSE_PRICE("prevClosePrice"),
    LAST_PRICE("lastPrice"),
    LAST_QTY("lastQty"),
    BID_PRICE("bidPrice"),
    BIG_QTY("bidQty"),
    ASK_PRICE("askPrice"),
    ASK_QTY("askQty"),
    OPEN_PRICE("openPrice"),
    HIGH_PRICE("highPrice"),
    LOW_PRICE("lowPrice"),
    VOLUME("volume"),
    QUOTE_VOLUME("quoteVolume"),
    OPEN_TIME("openTime"),
    CLOSE_TIME("closeTime"),
    FIRST_ID("firstId"),
    LAST_ID("lastId"),
    COUNT("count");

    private String value;

    DayStatisticFull(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}

public class TickerConverter {
    private static final DayStatisticFull[] keys = DayStatisticFull.values();

    public static List<Ticker> convertTickersFromString(Parser parser, String jsonString){
        List<JSONObject> jsonObjectList = parser.ParseJsonArray(jsonString);
        List<Ticker> tickerList = new ArrayList<>();

        Stream<JSONObject> productStream = Stream.of(jsonObjectList.toArray(new JSONObject[0]));
        productStream.forEach(p -> {
            Object jsonObjectKey;
            Ticker.TickerBuilder tickerBuilder = Ticker.build();
            for (DayStatisticFull key : keys) {
                jsonObjectKey = p.get(key.getValue());
                if (jsonObjectKey != null) {
                    switch (key) {
                        case SYMBOL:
                            tickerBuilder.setSymbol(jsonObjectKey.toString());
                            break;
                        case PRICE_CHANGE:
                            tickerBuilder.setPriceChange(Double.parseDouble(jsonObjectKey.toString()));
                            break;
                        case PRICE_CHANGE_PERCENT:
                            tickerBuilder.setPriceChangePercent(Double.parseDouble(jsonObjectKey.toString()));
                            break;
                        case WEIGHTED_AVG_PRICE:
                            tickerBuilder.setWeightedAvgPrice(Double.parseDouble(jsonObjectKey.toString()));
                            break;
                        case PREV_CLOSE_PRICE:
                            tickerBuilder.setPrevClosePrice(Double.parseDouble(jsonObjectKey.toString()));
                            break;
                        case LAST_PRICE:
                            tickerBuilder.setLastPrice(Double.parseDouble(jsonObjectKey.toString()));
                            break;
                        case LAST_QTY:
                            tickerBuilder.setLastQty(Double.parseDouble(jsonObjectKey.toString()));
                            break;
                        case BID_PRICE:
                            tickerBuilder.setBidPrice(Double.parseDouble(jsonObjectKey.toString()));
                            break;
                        case BIG_QTY:
                            tickerBuilder.setBidQty(Double.parseDouble(jsonObjectKey.toString()));
                            break;
                        case ASK_PRICE:
                            tickerBuilder.setAskPrice(Double.parseDouble(jsonObjectKey.toString()));
                            break;
                        case ASK_QTY:
                            tickerBuilder.setAskQty(Double.parseDouble(jsonObjectKey.toString()));
                            break;
                        case OPEN_PRICE:
                            tickerBuilder.setOpenPrice(Double.parseDouble(jsonObjectKey.toString()));
                            break;
                        case HIGH_PRICE:
                            tickerBuilder.setHighPrice(Double.parseDouble(jsonObjectKey.toString()));
                            break;
                        case LOW_PRICE:
                            tickerBuilder.setLowPrice(Double.parseDouble(jsonObjectKey.toString()));
                            break;
                        case VOLUME:
                            tickerBuilder.setVolume(Double.parseDouble(jsonObjectKey.toString()));
                            break;
                        case QUOTE_VOLUME:
                            tickerBuilder.setQuoteVolume(Double.parseDouble(jsonObjectKey.toString()));
                            break;
                        case OPEN_TIME:
                            tickerBuilder.setOpenTime(Long.parseLong(jsonObjectKey.toString()));
                            break;
                        case CLOSE_TIME:
                            tickerBuilder.setCloseTime(Long.parseLong(jsonObjectKey.toString()));
                            break;
                        case FIRST_ID:
                            tickerBuilder.setFirstId(Integer.parseInt(jsonObjectKey.toString()));
                            break;
                        case LAST_ID:
                            tickerBuilder.setLastId(Integer.parseInt(jsonObjectKey.toString()));
                            break;
                        case COUNT:
                            tickerBuilder.setCount(Integer.parseInt(jsonObjectKey.toString()));
                            break;
                    }
                }
            }
            tickerList.add(tickerBuilder.build());
        });

        return tickerList;
    }
}
