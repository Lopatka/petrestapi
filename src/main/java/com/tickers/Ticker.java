package com.tickers;

import java.util.Date;


public class Ticker {
    private String symbol;
    private double priceChange;
    private double priceChangePercent;
    private double weightedAvgPrice;
    private double prevClosePrice;
    private double lastPrice;
    private double lastQty;
    private double bidPrice;
    private double bidQty;
    private double askPrice;
    private double askQty;
    private double openPrice;
    private double highPrice;
    private double lowPrice;
    private double volume;
    private double quoteVolume;
    private long openTime;
    private long closeTime;
    private int firstId;
    private int lastId;
    private int count;

    public Ticker(){};

    public static TickerBuilder build(){
        return new TickerBuilder();
    }

    public String getSymbol() {
        return symbol;
    }

    public double getPriceChange() {
        return priceChange;
    }

    public double getPriceChangePercent() {
        return priceChangePercent;
    }

    public double getWeightedAvgPrice() {
        return weightedAvgPrice;
    }

    public double getPrevClosePrice() {
        return prevClosePrice;
    }

    public double getLastPrice() {
        return lastPrice;
    }

    public double getLastQty() {
        return lastQty;
    }

    public double getBidPrice() {
        return bidPrice;
    }

    public double getBidQty() {
        return bidQty;
    }

    public double getAskPrice() {
        return askPrice;
    }

    public double getAskQty() {
        return askQty;
    }

    public double getOpenPrice() {
        return openPrice;
    }

    public double getHighPrice() {
        return highPrice;
    }

    public double getLowPrice() {
        return lowPrice;
    }

    public double getVolume() {
        return volume;
    }

    public double getQuoteVolume() {
        return quoteVolume;
    }

    public long getOpenTime() {
        return openTime;
    }

    public long getCloseTime() {
        return closeTime;
    }

    public int getFirstId() {
        return firstId;
    }

    public int getLastId() {
        return lastId;
    }

    public int getCount() {
        return count;
    }

    public static class TickerBuilder {

        private Ticker ticker = new Ticker();

        public TickerBuilder setSymbol(String symbol) {
            ticker.symbol = symbol;
            return this;
        }

        public TickerBuilder setOpenPrice(double openPrice) {
            ticker.openPrice = openPrice;
            return this;
        }

        public TickerBuilder setHighPrice(double highPrice) {
            ticker.highPrice = highPrice;
            return this;
        }

        public TickerBuilder setLowPrice(double lowPrice) {
            ticker.lowPrice = lowPrice;
            return this;
        }

        public TickerBuilder setLastPrice(double lastPrice) {
            ticker.lastPrice = lastPrice;
            return this;
        }

        public TickerBuilder setVolume(double volume) {
            ticker.volume = volume;
            return this;
        }

        public TickerBuilder setQuoteVolume(double quoteVolume) {
            ticker.quoteVolume = quoteVolume;
            return this;
        }

        public TickerBuilder setOpenTime(long openTime) {
            ticker.openTime = openTime;
            return this;
        }

        public TickerBuilder setCloseTime(long closeTime) {
            ticker.closeTime = closeTime;
            return this;
        }

        public TickerBuilder setFirstId(int firstId) {
            ticker.firstId = firstId;
            return this;
        }

        public TickerBuilder setLastId(int lastId) {
            ticker.lastId = lastId;
            return this;
        }

        public TickerBuilder setCount(int count) {
            ticker.count = count;
            return this;
        }

        public TickerBuilder setPriceChange(double priceChange) {
            ticker.priceChange = priceChange;
            return this;
        }

        public TickerBuilder setPriceChangePercent(double priceChangePercent) {
            ticker.priceChangePercent = priceChangePercent;
            return this;
        }

        public TickerBuilder setWeightedAvgPrice(double weightedAvgPrice) {
            ticker.weightedAvgPrice = weightedAvgPrice;
            return this;
        }

        public TickerBuilder setPrevClosePrice(double prevClosePrice) {
            ticker.prevClosePrice = prevClosePrice;
            return this;
        }

        public TickerBuilder setLastQty(double lastQty) {
            ticker.lastQty = lastQty;
            return this;
        }

        public TickerBuilder setBidPrice(double bidPrice) {
            ticker.bidPrice = bidPrice;
            return this;
        }

        public TickerBuilder setBidQty(double bidQty) {
            ticker.bidQty = bidQty;
            return this;
        }

        public TickerBuilder setAskPrice(double askPrice) {
            ticker.askPrice = askPrice;
            return this;
        }

        public TickerBuilder setAskQty(double askQty) {
            ticker.askQty = askQty;
            return this;
        }

        public Ticker build(){
            return ticker;
        }
    }
}
