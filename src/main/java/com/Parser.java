package com;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.LinkedList;
import java.util.List;

public class Parser {
    private final JSONParser parser = new JSONParser();

    public List<JSONObject> ParseJsonArray(String jsonString) {
        List<JSONObject> resultObj = new LinkedList<>();
        JSONArray jsonArray;

        try {
            jsonArray = (JSONArray) parser.parse(jsonString);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        for (Object product : jsonArray) {
            JSONObject item = (JSONObject) product;
            resultObj.add(item);
        }
        return resultObj;
    }

    public JSONObject ParseJsonObject(String jsonString){
        JSONObject jsonObject;

        try {
            jsonObject = (JSONObject) parser.parse(jsonString);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        return  jsonObject;
    }
}
