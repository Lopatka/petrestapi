package com;

final public class Constants {
    final public static String ENDPOINT_TICKER_24HR = "https://api2.binance.com/api/v3/ticker/24hr";

    final public static String ENDPOINT_TICKER_BOOK_TICKER = "https://api2.binance.com/api/v3/ticker/bookTicker";
    private Constants() {}

}
