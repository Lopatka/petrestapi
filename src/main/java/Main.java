import com.Parser;
import com.RestApiClient;
import com.endpoints.Endpoint24hr;
import com.tickers.Ticker;
import com.tickers.TickerConverter;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;


public class Main {
    public static void main(String[] args) throws IOException {
        Endpoint24hr endpoint24hr = new Endpoint24hr();
        endpoint24hr.setTypeMini();
        List<Ticker> listTicker = new LinkedList<>();
        String endpoint = endpoint24hr.getEndpoint(new String[]{"BTCUSDT", "LTCUSDT"});
        System.out.println(endpoint);
        System.out.println(RestApiClient.getData(endpoint));

        try {
            listTicker = TickerConverter.convertTickersFromString(new Parser(), RestApiClient.getData(endpoint));
        } catch (Exception e) {
            System.out.println(e);
        }

        for (Ticker ticker : listTicker) {
            System.out.println(ticker.getSymbol());
        }
    }
}
